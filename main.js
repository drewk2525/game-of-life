let grid;
let nextGrid;
let cols;
let rows;
let generations = 0;
let resolution = 20;
let frames = 6;
let looping = false;

function setup() {
    createCanvas(1200, 800);
    frameRate(frames);
    cols = width / resolution;
    rows = height / resolution;
    start();
}

const make2DArray = (populate = true) => {
    const g = new Array(cols);
    for (let i = 0; i < cols; i++) {
        g[i] = new Array(rows);
        for (let j = 0; j < rows; j++) {
            if (populate) {
                g[i][j] = 0;
            }
        }
    }
    return g;
};

function mouseClicked() {
    const x = floor(mouseX / resolution);
    const y = floor(mouseY / resolution);
    if (grid[x] == undefined || !grid[x][y] == undefined) {
        return;
    }
    const newValue = grid[x][y] === 0 ? 1 : 0;
    grid[x][y] = newValue;
    draw();
}

function keyPressed() {
    const SPACEBAR = 32;
    const PLUS = 187;
    const MINUS = 189;
    if (keyCode === SPACEBAR) {
        if (looping) {
            noLoop();
        } else {
            loop();
        }
        looping = !looping;
    }
    if (keyCode === PLUS) {
        frames += 2;
        draw();
    }
    if (keyCode === MINUS) {
        if (frames > 2) {
            frames -= 2;
            draw();
        }
    }
}

const start = () => {
    grid = make2DArray();
    noLoop();
};

function draw() {
    frameRate(frames);
    background(0);
    nextGrid = make2DArray(false);
    for (let x = 0; x < cols; x++) {
        for (let y = 0; y < rows; y++) {
            const fillValue = grid[x][y] === 0 ? "#65451F" : "#4F6F52";
            const neighborCount = checkNeighbors(x, y);
            const state = grid[x][y];
            const min = 3;
            const max = 3;
            if (looping) {
                if (
                    state == 0 &&
                    neighborCount >= min &&
                    neighborCount <= max
                ) {
                    nextGrid[x][y] = 1;
                } else if (
                    state == 1 &&
                    (neighborCount < 2 || neighborCount > max)
                ) {
                    nextGrid[x][y] = 0;
                } else {
                    nextGrid[x][y] = state;
                }
            }
            fill(fillValue);
            stroke(0);
            rect(
                x * resolution,
                y * resolution,
                resolution - 1,
                resolution - 1
            );
        }
    }
    textSize(22);
    textAlign(RIGHT, CENTER);
    fill("#ff0000");
    const offset = 18;
    text(`${frames}`, width - offset, offset);
    if (looping) {
        generations++;
        grid = nextGrid;
    }
}

function checkNeighbors(x, y) {
    let neighbors = 0;
    for (let i = -1; i < 2; i++) {
        for (let j = -1; j < 2; j++) {
            col = (x + i + cols) % cols;
            row = (y + j + rows) % rows;
            neighbors += grid[col][row];
        }
    }
    return (neighbors -= grid[x][y]);
}
