# GAME OF LIFE

This is based on Conway's Game of Life: https://en.wikipedia.org/wiki/Conway's_Game_of_Life

## Run

To run the application, simply open the index.html file in a browser

## Controls

Space Bar - Pause or unpause
Left Click - Create the inverse state of the cell under the mouse
Minus Key (-) - Slow down the frame rate by 2
Plus Key (+) - Speed up the frame rate by 2
